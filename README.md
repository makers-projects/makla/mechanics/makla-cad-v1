# makla-cad-v1

This repo contains the CAD files for the mechanical parts of the `makla` project.

It depends on `BOSL2`, which can be obtained e.g. by adding `--recurse-submodules` to `git clone`.

The overarching documentation of the project is located in the [makla-meta](https://gitlab.com/makers-projects/makla/makla-meta) repo.
