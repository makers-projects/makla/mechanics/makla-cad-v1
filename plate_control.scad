$fa=1;
$fs=0.3;

include <BOSL2/std.scad>

// CONSTANTS

PCBMOUNT_DEPTH = 6;
PCBMOUNT_DIAM = 7;
PCBMOUNT_INSERT_DIAM = 4.5;
PCBMOUNT_INSERT_DEPTH = 8;
BRACE_DEPTH = 3;

PITCH_CHANNELS = 24;
BRACE_X_OFFSET_1x3 = 3.5;
BRACE_X_WIDTH_1x3 = (PITCH_CHANNELS - 21);

hole_offsets_1x3 = [
[0, 0],
[0, 25],
[-7, 55],
[0, 79]
];

brace_y_offsets_1x3 = [
[-6.1, -1.1],
[21.1, 28.9],
[51.1, 58.9],
[81.1, 86.1]
];

PITCH_2x1 = 40;
BRACE_X_OFFSET_2x1 = 4.0;
BRACE_X_WIDTH_2x1 = (PITCH_2x1 - 36);

hole_offsets_2x1 = [
[0, 0],
[0, 20],
[-27.6, 23]
];

brace_y_offsets_2x1 = [
[-6.1, -1.1],
[21.1, 31.1]
];

// CALCULATED

brace_hor_len_1x3 = PITCH_CHANNELS * 8;
brace_ver_len_1x3 = brace_y_offsets_1x3[3][1] - brace_y_offsets_1x3[0][0];

brace_hor_len_2x1 = PITCH_2x1 * 5;
brace_ver_len_2x1 = brace_y_offsets_2x1[1][1] - brace_y_offsets_2x1[0][0];

// MODULES

module plate_1x3s() {
    difference() {
        union() {
            for (nr_ch = [0:7]) {
                translate([nr_ch * PITCH_CHANNELS, 0, 0]) {
                    for (nr_hole = [0:3]) {
                        translate([hole_offsets_1x3[nr_hole][0], hole_offsets_1x3[nr_hole][1], 0]) {
                            cylinder(h=PCBMOUNT_DEPTH, d=PCBMOUNT_DIAM);
                        }
                    }
                }
            }
            
            for (nr_ch = [-1:7]) {
                translate([nr_ch * PITCH_CHANNELS, 0, 0]) {
                    translate([BRACE_X_OFFSET_1x3, brace_y_offsets_1x3[0][0], 0]) cube([BRACE_X_WIDTH_1x3, brace_ver_len_1x3, BRACE_DEPTH]);
                }
            }
            
            for (nr_brace = [0:3]) {
                translate([-PITCH_CHANNELS + BRACE_X_OFFSET_1x3, brace_y_offsets_1x3[nr_brace][0], 0]) cube([brace_hor_len_1x3, brace_y_offsets_1x3[nr_brace][1] - brace_y_offsets_1x3[nr_brace][0], BRACE_DEPTH]);
            }
        }
        for (nr_ch = [0:7]) {
            translate([nr_ch * PITCH_CHANNELS, 0, 0]) {
                for (nr_hole = [0:3]) {
                    translate([hole_offsets_1x3[nr_hole][0], hole_offsets_1x3[nr_hole][1], 0]) {
                        cylinder(h=PCBMOUNT_INSERT_DEPTH, d=PCBMOUNT_INSERT_DIAM);
                    }
                }
            }
        }
    }
}

module plate_2x1s() {
    difference() {
        union() {
            for (nr_module = [0:4]) {
                translate([nr_module * PITCH_2x1, 0, 0]) {
                    for (nr_hole = [0:2]) {
                        translate([hole_offsets_2x1[nr_hole][0], hole_offsets_2x1[nr_hole][1], 0]) {
                           cylinder(h=PCBMOUNT_DEPTH, d=PCBMOUNT_DIAM);
                        }
                    }
                }
            }
            
            for (nr_module = [-1:4]) {
                translate([nr_module * PITCH_2x1, 0, 0]) {
                    translate([BRACE_X_OFFSET_2x1, brace_y_offsets_2x1[0][0], 0]) cube ([BRACE_X_WIDTH_2x1, brace_ver_len_2x1, BRACE_DEPTH]);
                }
            }
            
            for (nr_brace = [0:1]) {
                translate([-PITCH_2x1 + BRACE_X_OFFSET_2x1, brace_y_offsets_2x1[nr_brace][0], 0]) cube([brace_hor_len_2x1, brace_y_offsets_2x1[nr_brace][1] - brace_y_offsets_2x1[nr_brace][0], BRACE_DEPTH]);
            }
        }
        
        for (nr_module = [0:4]) {
            translate([nr_module * PITCH_2x1, 0, 0]) {
                for (nr_hole = [0:2]) {
                    translate([hole_offsets_2x1[nr_hole][0], hole_offsets_2x1[nr_hole][1], 0]) {
                       cylinder(h=PCBMOUNT_INSERT_DEPTH, d=PCBMOUNT_INSERT_DIAM);
                    }
                }
            }
        }
        
        translate([hole_offsets_2x1[2][0] - (PCBMOUNT_DIAM / 2), hole_offsets_2x1[2][1] - (PCBMOUNT_DIAM / 2), 0]) cube([PCBMOUNT_DIAM, 1, PCBMOUNT_DEPTH]);
    }
}

module plate_control() {
    shift_x_2x1s = (-PITCH_CHANNELS + BRACE_X_OFFSET_1x3) - (-PITCH_2x1 + BRACE_X_OFFSET_2x1);
    plate_1x3s();
    translate([shift_x_2x1s, brace_ver_len_1x3, 0]) plate_2x1s();
}

plate_control();
