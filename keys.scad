$fa=1;
$fs=0.3;


include <BOSL2/std.scad>


WHITE_PITCH = 22;
BLACK_PITCH_2GROUP = 25.2;
BLACK_PITCH_3GROUP = 24.3;
GAP_WIDTH = 1;
WHITE_WIDTH = WHITE_PITCH - GAP_WIDTH;
BLACK_WIDTH = 11;
WHITE_LEN_OPEN = 150;
BLACK_LEN_OPEN = 100;
WHITE_HEIGHT = 17.8;
BLACK_HEIGHT_EXTRA = 10.5;
MOUNTING_LEN = 55;
OPPOSITE_LEN = 70;
MOUNTHOLE_DIAM = 10.20;
WHITE_LEN = WHITE_LEN_OPEN + MOUNTING_LEN + OPPOSITE_LEN;
BLACK_LEN = BLACK_LEN_OPEN + MOUNTING_LEN + OPPOSITE_LEN;
WHITE_LEN_STARTTOMOUNT = WHITE_LEN_OPEN + MOUNTING_LEN;
BLACK_LEN_STARTTOMOUNT = BLACK_LEN_OPEN + MOUNTING_LEN;
BLACK_HEIGHT = WHITE_HEIGHT + BLACK_HEIGHT_EXTRA;
HIDDEN_LEN = MOUNTING_LEN + OPPOSITE_LEN;
EDGE_HEIGHT = 2;
EDGE_WEDGE_HEIGHT = WHITE_HEIGHT - EDGE_HEIGHT;
EDGE_WEDGE_DEPTH = 10;
BACK_EDGE_WEDGE_DEPTH = 5;
BLACK_ANGLE = 45;
ROUNDING_RADIUS = 2;
MOUNT_WIDTH = 3;
WALL_THICKNESS = 2;
CUTOUT_MARGIN_MOUNTHOLE = 10;
CUTOUT_MARGIN_HIDDEN = 2;
CONTACTHOLE_DIAM = 6;
SPRINGHOLE_DIAM = 3;
CONTACTHOLE_MARGIN = 2;
SPRINGHOLE_OFFSET = 18;


black_cutout_offset = -BLACK_WIDTH / 2 + WALL_THICKNESS;
black_cutout_width = BLACK_WIDTH - 2 * WALL_THICKNESS;

c_width = WHITE_PITCH - (MOUNT_WIDTH / 2 + GAP_WIDTH) - (BLACK_PITCH_2GROUP / 2 + BLACK_WIDTH / 2 + GAP_WIDTH - WHITE_PITCH / 2);
c_cutout_offset = -WHITE_PITCH / 2 + (MOUNT_WIDTH / 2 + GAP_WIDTH) + WALL_THICKNESS;
c_cutout_width = c_width - 2 * WALL_THICKNESS;

d_width = BLACK_PITCH_2GROUP - BLACK_WIDTH - 2 * GAP_WIDTH;
d_cutout_offset = -d_width / 2 + WALL_THICKNESS;
d_cutout_width = d_width - 2 * WALL_THICKNESS;

e_width = WHITE_PITCH - (MOUNT_WIDTH / 2 + GAP_WIDTH) - (BLACK_PITCH_2GROUP / 2 + BLACK_WIDTH / 2 + GAP_WIDTH - WHITE_PITCH / 2);
e_cutout_offset = -WHITE_PITCH + (BLACK_PITCH_2GROUP / 2 + BLACK_WIDTH / 2 + GAP_WIDTH) + WALL_THICKNESS;
e_cutout_width = e_width - 2 * WALL_THICKNESS;

f_width = WHITE_PITCH - (MOUNT_WIDTH / 2 + GAP_WIDTH) - (BLACK_PITCH_3GROUP + BLACK_WIDTH / 2 + GAP_WIDTH - WHITE_PITCH);
f_cutout_offset = -WHITE_PITCH / 2 + (MOUNT_WIDTH / 2 + GAP_WIDTH) + WALL_THICKNESS;
f_cutout_width = f_width - 2 * WALL_THICKNESS;

g_width = BLACK_PITCH_3GROUP - BLACK_WIDTH - 2 * GAP_WIDTH;
g_cutout_offset = WHITE_PITCH / 2 - BLACK_PITCH_3GROUP + BLACK_WIDTH / 2 + GAP_WIDTH + WALL_THICKNESS;
g_cutout_width = g_width - 2 * WALL_THICKNESS;

a_width = BLACK_PITCH_3GROUP - BLACK_WIDTH - 2 * GAP_WIDTH;
a_cutout_offset = -WHITE_PITCH / 2 + BLACK_WIDTH / 2 + GAP_WIDTH + WALL_THICKNESS;
a_cutout_width = a_width - 2 * WALL_THICKNESS;

h_width = WHITE_PITCH - (MOUNT_WIDTH / 2 + GAP_WIDTH) - (BLACK_PITCH_3GROUP + BLACK_WIDTH / 2 + GAP_WIDTH - WHITE_PITCH);
h_cutout_offset = -WHITE_PITCH * 1.5 + BLACK_PITCH_3GROUP + BLACK_WIDTH / 2 + GAP_WIDTH + WALL_THICKNESS;
h_cutout_width = h_width - 2 * WALL_THICKNESS;


module key_white() {
    difference() {
        translate([-WHITE_WIDTH / 2, -WHITE_LEN_STARTTOMOUNT, 0]) diff() cube([WHITE_WIDTH, WHITE_LEN, WHITE_HEIGHT]) {edge_mask([TOP,"Z"],except=[BACK])
        rounding_edge_mask(l=max($parent_size)*1.01,r=ROUNDING_RADIUS);}
        translate([-WHITE_WIDTH / 2, 0, WHITE_HEIGHT / 2]) rotate([0, 90, 0]) cylinder(h = WHITE_WIDTH, d = MOUNTHOLE_DIAM);
        translate([-WHITE_WIDTH / 2, -WHITE_LEN_STARTTOMOUNT, 0]) 
    wedge([WHITE_WIDTH, EDGE_WEDGE_DEPTH, EDGE_WEDGE_DEPTH]);
        translate([WHITE_WIDTH / 2, OPPOSITE_LEN, 0]) rotate([0, 0, 180]) 
    wedge([WHITE_WIDTH, BACK_EDGE_WEDGE_DEPTH, BACK_EDGE_WEDGE_DEPTH]);
        translate([-WHITE_WIDTH / 2, OPPOSITE_LEN, WHITE_HEIGHT]) rotate([180, 0, 0]) 
    wedge([WHITE_WIDTH, BACK_EDGE_WEDGE_DEPTH, BACK_EDGE_WEDGE_DEPTH]);
        translate([-WHITE_WIDTH / 2, OPPOSITE_LEN - (CONTACTHOLE_MARGIN + CONTACTHOLE_DIAM / 2), WHITE_HEIGHT / 2]) rotate([0, 90, 0]) cylinder(h = WHITE_WIDTH, d = CONTACTHOLE_DIAM);
        translate([-WHITE_WIDTH / 2, - (MOUNTING_LEN - SPRINGHOLE_OFFSET), WHITE_HEIGHT / 2]) rotate([0, 90, 0]) cylinder(h = WHITE_WIDTH, d = SPRINGHOLE_DIAM);
    }
}

module key_black() {
    difference() {
        translate([-BLACK_WIDTH / 2, -BLACK_LEN_STARTTOMOUNT, 0]) diff() cube([BLACK_WIDTH, BLACK_LEN, BLACK_HEIGHT]) {edge_mask([TOP],except=[BACK])
        rounding_edge_mask(l=max($parent_size)*1.01,r=ROUNDING_RADIUS);}
        translate([-WHITE_WIDTH / 2, 0, WHITE_HEIGHT / 2]) rotate([0, 90, 0]) cylinder(h = WHITE_WIDTH, d = MOUNTHOLE_DIAM);
        translate([-BLACK_WIDTH / 2, -MOUNTING_LEN, WHITE_HEIGHT]) cube([BLACK_WIDTH, HIDDEN_LEN, BLACK_HEIGHT_EXTRA]);
        translate([0, -BLACK_LEN_STARTTOMOUNT, WHITE_HEIGHT]) rotate([0, 90, 0]) rotate([0, 0, -BLACK_ANGLE]) cuboid([BLACK_HEIGHT_EXTRA * 2, BLACK_HEIGHT_EXTRA * 2, BLACK_WIDTH], anchor=RIGHT+BACK, rounding=-ROUNDING_RADIUS);
        translate([-BLACK_WIDTH / 2, -BLACK_LEN_STARTTOMOUNT, 0]) 
    wedge([BLACK_WIDTH, EDGE_WEDGE_DEPTH, EDGE_WEDGE_DEPTH]);
        translate([BLACK_WIDTH / 2, OPPOSITE_LEN, 0]) rotate([0, 0, 180]) 
    wedge([BLACK_WIDTH, BACK_EDGE_WEDGE_DEPTH, BACK_EDGE_WEDGE_DEPTH]);
        translate([-BLACK_WIDTH / 2, OPPOSITE_LEN, WHITE_HEIGHT]) rotate([180, 0, 0]) 
    wedge([BLACK_WIDTH, BACK_EDGE_WEDGE_DEPTH, BACK_EDGE_WEDGE_DEPTH]);
        translate([-BLACK_WIDTH / 2, OPPOSITE_LEN - (CONTACTHOLE_MARGIN + CONTACTHOLE_DIAM / 2), WHITE_HEIGHT / 2]) rotate([0, 90, 0]) cylinder(h = BLACK_WIDTH, d = CONTACTHOLE_DIAM);
        translate([-BLACK_WIDTH / 2, - (MOUNTING_LEN - SPRINGHOLE_OFFSET), WHITE_HEIGHT / 2]) rotate([0, 90, 0]) cylinder(h = BLACK_WIDTH, d = SPRINGHOLE_DIAM);
        translate([black_cutout_offset, -(MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE), BLACK_HEIGHT]) rotate([180, 0, 0]) cube([black_cutout_width, MOUNTING_LEN - MOUNTHOLE_DIAM / 2 - CUTOUT_MARGIN_HIDDEN - CUTOUT_MARGIN_MOUNTHOLE, BLACK_HEIGHT]);
        translate([black_cutout_offset, MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE, 0]) cube([black_cutout_width, OPPOSITE_LEN, BLACK_HEIGHT]);
    }
}

module cutout_black() {
    translate([-(BLACK_WIDTH + 2 * GAP_WIDTH) / 2, -(BLACK_LEN_STARTTOMOUNT + GAP_WIDTH), 0]) cube([BLACK_WIDTH + 2 * GAP_WIDTH, BLACK_LEN + GAP_WIDTH, BLACK_HEIGHT]);
}

module cutout_mount() {
    translate([-(MOUNT_WIDTH + 2 * GAP_WIDTH) / 2, -MOUNTING_LEN, 0]) cube([MOUNT_WIDTH + 2 * GAP_WIDTH, HIDDEN_LEN, BLACK_HEIGHT]);
}

module key_c() {
    difference() {
        key_white();
        translate([WHITE_PITCH - (BLACK_PITCH_2GROUP / 2), 0, 0]) cutout_black();
        translate([-WHITE_PITCH / 2, 0, 0]) cutout_mount();
        translate([c_cutout_offset, -(MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE), BLACK_HEIGHT]) rotate([180, 0, 0]) cube([c_cutout_width, MOUNTING_LEN - MOUNTHOLE_DIAM / 2 - CUTOUT_MARGIN_HIDDEN - CUTOUT_MARGIN_MOUNTHOLE, BLACK_HEIGHT]);
        translate([c_cutout_offset, MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE, 0]) cube([c_cutout_width, OPPOSITE_LEN, BLACK_HEIGHT]);
    }
}

module key_d() {
    difference() {
        key_white();
        translate([-(BLACK_PITCH_2GROUP / 2), 0, 0]) cutout_black();
        translate([BLACK_PITCH_2GROUP / 2, 0, 0]) cutout_black();
        translate([d_cutout_offset, -(MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE), BLACK_HEIGHT]) rotate([180, 0, 0]) cube([d_cutout_width, MOUNTING_LEN - MOUNTHOLE_DIAM / 2 - CUTOUT_MARGIN_HIDDEN - CUTOUT_MARGIN_MOUNTHOLE, BLACK_HEIGHT]);
        translate([d_cutout_offset, MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE, 0]) cube([d_cutout_width, OPPOSITE_LEN, BLACK_HEIGHT]);
    }
}

module key_e() {
    difference() {
        key_white();
        translate([-WHITE_PITCH + (BLACK_PITCH_2GROUP / 2), 0, 0]) cutout_black();
        translate([WHITE_PITCH / 2, 0, 0]) cutout_mount();
        translate([e_cutout_offset, -(MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE), BLACK_HEIGHT]) rotate([180, 0, 0]) cube([e_cutout_width, MOUNTING_LEN - MOUNTHOLE_DIAM / 2 - CUTOUT_MARGIN_HIDDEN - CUTOUT_MARGIN_MOUNTHOLE, BLACK_HEIGHT]);
        translate([e_cutout_offset, MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE, 0]) cube([e_cutout_width, OPPOSITE_LEN, BLACK_HEIGHT]);
    }
}

module key_f() {
    difference() {
        key_white();
        translate([WHITE_PITCH * 1.5 - BLACK_PITCH_3GROUP, 0, 0]) cutout_black();
        translate([-WHITE_PITCH / 2, 0, 0]) cutout_mount();
        translate([f_cutout_offset, -(MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE), BLACK_HEIGHT]) rotate([180, 0, 0]) cube([f_cutout_width, MOUNTING_LEN - MOUNTHOLE_DIAM / 2 - CUTOUT_MARGIN_HIDDEN - CUTOUT_MARGIN_MOUNTHOLE, BLACK_HEIGHT]);
        translate([f_cutout_offset, MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE, 0]) cube([f_cutout_width, OPPOSITE_LEN, BLACK_HEIGHT]);
    }
}

module key_g() {
    difference() {
        key_white();
        translate([WHITE_PITCH / 2 - BLACK_PITCH_3GROUP, 0, 0]) cutout_black();
        translate([WHITE_PITCH / 2, 0, 0]) cutout_black();
        translate([g_cutout_offset, -(MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE), BLACK_HEIGHT]) rotate([180, 0, 0]) cube([g_cutout_width, MOUNTING_LEN - MOUNTHOLE_DIAM / 2 - CUTOUT_MARGIN_HIDDEN - CUTOUT_MARGIN_MOUNTHOLE, BLACK_HEIGHT]);
        translate([g_cutout_offset, MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE, 0]) cube([g_cutout_width, OPPOSITE_LEN, BLACK_HEIGHT]);
    }
}

module key_a() {
    difference() {
        key_white();
        translate([-WHITE_PITCH / 2, 0, 0]) cutout_black();
        translate([-WHITE_PITCH / 2 + BLACK_PITCH_3GROUP, 0, 0]) cutout_black();
        translate([a_cutout_offset, -(MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE), BLACK_HEIGHT]) rotate([180, 0, 0]) cube([a_cutout_width, MOUNTING_LEN - MOUNTHOLE_DIAM / 2 - CUTOUT_MARGIN_HIDDEN - CUTOUT_MARGIN_MOUNTHOLE, BLACK_HEIGHT]);
        translate([a_cutout_offset, MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE, 0]) cube([a_cutout_width, OPPOSITE_LEN, BLACK_HEIGHT]);
    }
}

module key_h() {
    difference() {
        key_white();
        translate([-WHITE_PITCH * 1.5 + BLACK_PITCH_3GROUP, 0, 0]) cutout_black();
        translate([WHITE_PITCH / 2, 0, 0]) cutout_mount();
        translate([h_cutout_offset, -(MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE), BLACK_HEIGHT]) rotate([180, 0, 0]) cube([h_cutout_width, MOUNTING_LEN - MOUNTHOLE_DIAM / 2 - CUTOUT_MARGIN_HIDDEN - CUTOUT_MARGIN_MOUNTHOLE, BLACK_HEIGHT]);
        translate([h_cutout_offset, MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE, 0]) cube([h_cutout_width, OPPOSITE_LEN, BLACK_HEIGHT]);
    }
}

module key_c_finish() {
    difference() {
        key_white();
        intersection() {
            translate([WHITE_PITCH - (BLACK_PITCH_2GROUP / 2), 0, 0]) cutout_black();
            scale([10, 1, 1]) cutout_mount();
        }
        translate([-WHITE_PITCH / 2, 0, 0]) cutout_mount();
        translate([c_cutout_offset, -(MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE), BLACK_HEIGHT]) rotate([180, 0, 0]) cube([c_cutout_width, MOUNTING_LEN - MOUNTHOLE_DIAM / 2 - CUTOUT_MARGIN_HIDDEN - CUTOUT_MARGIN_MOUNTHOLE, BLACK_HEIGHT]);
        translate([c_cutout_offset, MOUNTHOLE_DIAM / 2 + CUTOUT_MARGIN_MOUNTHOLE, 0]) cube([c_cutout_width, OPPOSITE_LEN, BLACK_HEIGHT]);
    }
}


module octave() {
    translate([WHITE_PITCH * 0, 0, 0]) key_c();

    translate([WHITE_PITCH * 0 + WHITE_PITCH -(BLACK_PITCH_2GROUP / 2), 0, 0]) key_black();

    translate([WHITE_PITCH * 1, 0, 0]) key_d();

    translate([WHITE_PITCH * 0 + WHITE_PITCH + BLACK_PITCH_2GROUP / 2, 0, 0]) key_black();

    translate([WHITE_PITCH * 2, 0, 0]) key_e();

    translate([WHITE_PITCH * 3, 0, 0]) key_f();

    translate([WHITE_PITCH * 3 + WHITE_PITCH * 1.5 - BLACK_PITCH_3GROUP, 0, 0]) key_black();

    translate([WHITE_PITCH * 4, 0, 0]) key_g();

    translate([WHITE_PITCH * 4 + WHITE_PITCH / 2, 0, 0]) key_black();

    translate([WHITE_PITCH * 5, 0, 0]) key_a();

    translate([WHITE_PITCH * 5 - WHITE_PITCH / 2 + BLACK_PITCH_3GROUP, 0, 0]) key_black();

    translate([WHITE_PITCH * 6, 0, 0]) key_h();
}

MOUNT_LENGTH_BOTTOM = 70;
MOUNT_LENGTH_TOP = MOUNTHOLE_DIAM * 3;
MOUNT_STAND_THICKNESS = 4;
MOUNT_STAND_WIDTH = 25;
MOUNT_HEIGHT_HOLE = 40;
MOUNT_HEIGHT_COMPLETE = MOUNT_HEIGHT_HOLE + MOUNTHOLE_DIAM;
MOUNT_SCREWHOLES_EDGEDIST = 15;
MOUNT_SCREWHOLES_DIAM = 6;
MOUNT_SCREWHOLES_KEEPOUT_DIAM = 20;
MOUNT_MOUNTHOLE_CLEARANCE = (WHITE_HEIGHT / 2) * 1.5;
MOUNT_TALL_HOLE_PITCH = 30;


module mount(tall = false) {
    difference() {
        cube([MOUNT_WIDTH, MOUNT_LENGTH_BOTTOM, MOUNT_HEIGHT_COMPLETE]);
         translate([20, 0, MOUNT_HEIGHT_COMPLETE]) rotate([0, 180, 0]) wedge([80, (MOUNT_LENGTH_BOTTOM - MOUNT_LENGTH_TOP) / 2, MOUNT_HEIGHT_COMPLETE - MOUNT_STAND_THICKNESS]);
        translate([-20, MOUNT_LENGTH_BOTTOM, MOUNT_HEIGHT_COMPLETE]) rotate([0, 180, 180]) wedge([80, (MOUNT_LENGTH_BOTTOM - MOUNT_LENGTH_TOP) / 2, MOUNT_HEIGHT_COMPLETE - MOUNT_STAND_THICKNESS]);
        translate([-10, MOUNT_LENGTH_BOTTOM / 2, MOUNT_HEIGHT_HOLE]) rotate([0, 90, 0]) cylinder(20, d = MOUNTHOLE_DIAM);
    }
    if (tall) {
        difference() {
            translate([0, (MOUNT_LENGTH_BOTTOM - MOUNT_LENGTH_TOP) / 2, MOUNT_HEIGHT_COMPLETE]) cuboid([MOUNT_WIDTH, MOUNT_LENGTH_TOP, MOUNT_TALL_HOLE_PITCH], anchor = FRONT+LEFT+BOTTOM, chamfer = MOUNT_LENGTH_TOP / 2 - 0.7 * MOUNTHOLE_DIAM, edges = [TOP+FRONT, TOP+BACK]);
        translate([-10, MOUNT_LENGTH_BOTTOM / 2, MOUNT_HEIGHT_HOLE + MOUNT_TALL_HOLE_PITCH]) rotate([0, 90, 0]) cylinder(20, d = MOUNTHOLE_DIAM);
        }
    }
    difference() {
        cube([MOUNT_STAND_WIDTH, MOUNT_LENGTH_BOTTOM, MOUNT_STAND_THICKNESS]);
        translate([(MOUNT_STAND_WIDTH + MOUNT_WIDTH) / 2, MOUNT_SCREWHOLES_EDGEDIST, -10]) #cylinder(20, d=MOUNT_SCREWHOLES_DIAM);
        translate([(MOUNT_STAND_WIDTH + MOUNT_WIDTH) / 2, MOUNT_LENGTH_BOTTOM - MOUNT_SCREWHOLES_EDGEDIST, -10]) #cylinder(20, d=MOUNT_SCREWHOLES_DIAM);
    }
    translate([MOUNT_WIDTH, MOUNT_LENGTH_BOTTOM / 2, MOUNT_STAND_THICKNESS]) rotate([0, 0, -90]) wedge([MOUNT_LENGTH_BOTTOM - 2 * MOUNT_SCREWHOLES_EDGEDIST - MOUNT_SCREWHOLES_KEEPOUT_DIAM, MOUNT_STAND_WIDTH - MOUNT_WIDTH, MOUNT_HEIGHT_HOLE - MOUNT_MOUNTHOLE_CLEARANCE - MOUNT_STAND_THICKNESS], anchor=BOTTOM+FRONT+CENTER);
}

SPIKES_FRONT_THICKNESS = 3;
SPIKES_FRONT_LENGTH = BLACK_HEIGHT;
SPIKES_FRONT_SLACK = 0.6;
SPIKES_FRONT_PLATE_HEIGHT = 30;
SPIKES_FRONT_PLATE_WIDTH = 7 * WHITE_PITCH - 2 * GAP_WIDTH;
SPIKES_FRONT_ROUNDING = 3;

key_offsets = [
    0,
    WHITE_PITCH - (BLACK_PITCH_2GROUP / 2),
    WHITE_PITCH,
    WHITE_PITCH + BLACK_PITCH_2GROUP / 2,
    WHITE_PITCH * 2,
    WHITE_PITCH * 3,
    WHITE_PITCH * 3 + WHITE_PITCH * 1.5 - BLACK_PITCH_3GROUP,
    WHITE_PITCH * 4,
    WHITE_PITCH * 4 + WHITE_PITCH / 2,
    WHITE_PITCH * 5,
    WHITE_PITCH * 5 - WHITE_PITCH / 2 + BLACK_PITCH_3GROUP,
    WHITE_PITCH * 6
];

module spikes_front() {
    difference() {
        translate([-(WHITE_PITCH / 2 - GAP_WIDTH), 0, 0]) cuboid([SPIKES_FRONT_PLATE_WIDTH, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_PLATE_HEIGHT], anchor=BOTTOM+LEFT+FRONT, rounding=3, edges=[BOTTOM+LEFT, BOTTOM+RIGHT, TOP+LEFT, TOP+RIGHT]);
        for (i=[0:3:6]) translate([i * WHITE_PITCH, 10, SPIKES_FRONT_PLATE_HEIGHT / 2]) rotate([90, 0, 0]) cylinder(20, d=MOUNT_SCREWHOLES_DIAM);
    }
    
    translate([0, 0, -SPIKES_FRONT_LENGTH]) {
        translate([key_offsets[0], 0, 0]) translate([c_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([c_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[1], 0, 0]) translate([black_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([black_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[2], 0, 0]) translate([d_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([d_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[3], 0, 0]) translate([black_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([black_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[4], 0, 0]) translate([e_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([e_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[5], 0, 0]) translate([f_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([f_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[6], 0, 0]) translate([black_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([black_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[7], 0, 0]) translate([g_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([g_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[8], 0, 0]) translate([black_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([black_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[9], 0, 0]) translate([a_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([a_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[10], 0, 0]) translate([black_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([black_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);

        translate([key_offsets[11], 0, 0]) translate([h_cutout_offset + SPIKES_FRONT_SLACK / 2, 0, 0]) cuboid([h_cutout_width - SPIKES_FRONT_SLACK, SPIKES_FRONT_THICKNESS, SPIKES_FRONT_LENGTH], anchor=BOTTOM+LEFT+FRONT, rounding=SPIKES_FRONT_ROUNDING,
    edges=[BOTTOM+LEFT, BOTTOM+RIGHT]);
    }
}

PICKUP_CONTACT_STROKE = 2;
PICKUP_CONTACT_SLACK = 2;
PICKUP_CORE_HEIGHT = CONTACTHOLE_DIAM + PICKUP_CONTACT_SLACK;
PICKUP_COVER_HEIGHT = 6;
PICKUP_DEPTH = 40;
PICKUP_SCREWHOLE_DIAM = 6;
PICKUP_SCREWHOLE_OFFSET = 6;
PICKUP_GROOVE_DIAM = 1;
PICKUP_EXTRA_LENGTH = WHITE_PITCH / 3;
PICKUP_LENGTH = WHITE_PITCH * 6 + PICKUP_EXTRA_LENGTH * 2;

PICKUP_STAND_HEIGHT = MOUNT_HEIGHT_HOLE - (CONTACTHOLE_DIAM / 2) - PICKUP_COVER_HEIGHT + PICKUP_CONTACT_STROKE;
PICKUP_STAND_BOTTOM_THICKNESS = 4;
PICKUP_STAND_SCREWHOLES_DIAM = 6;
PICKUP_STAND_TOPHOLES_DIST = 3;

pickup_screw_offsets = [
    key_offsets[1] / 2,
    (key_offsets[4] + key_offsets[5]) / 2,
    (key_offsets[7] + key_offsets[8]) / 2,
    (key_offsets[10] + key_offsets[11]) / 2
];

module pickup_screw_cutout() {
    for (offset_length = pickup_screw_offsets) for (offset_depth = [PICKUP_SCREWHOLE_OFFSET, PICKUP_DEPTH - PICKUP_SCREWHOLE_OFFSET]) translate([offset_length, offset_depth, 0]) cylinder(h=PICKUP_CORE_HEIGHT + PICKUP_COVER_HEIGHT * 2, d=PICKUP_SCREWHOLE_DIAM);
}

module pickup_core() {
    difference() {
        translate([-PICKUP_EXTRA_LENGTH, 0, 0]) cube([PICKUP_LENGTH, PICKUP_DEPTH, PICKUP_CORE_HEIGHT]);
        pickup_screw_cutout();
    }
}

module pickup_cover_bottom() {
    difference() {
        translate([-PICKUP_EXTRA_LENGTH, 0, 0]) cube([PICKUP_LENGTH, PICKUP_DEPTH, PICKUP_COVER_HEIGHT]);
        for (offset_groove = key_offsets) {
            translate([offset_groove, 0, PICKUP_COVER_HEIGHT]) rotate([-90, 0, 0]) cylinder(h=PICKUP_DEPTH, d=PICKUP_GROOVE_DIAM);
        }
        pickup_screw_cutout();
    }
}

module pickup_cover_top() {
    difference() {
        translate([-PICKUP_EXTRA_LENGTH, 0, 0]) cube([PICKUP_LENGTH, PICKUP_DEPTH, PICKUP_COVER_HEIGHT]);
        for (offset_groove = key_offsets) {
            translate([offset_groove, 0, 0]) rotate([-90, 0, 0]) cylinder(h=PICKUP_DEPTH, d=PICKUP_GROOVE_DIAM);
        }
        pickup_screw_cutout();
    }
}

module pickup_stand() {
    offset_cutout_1 = pickup_screw_offsets[0] + PICKUP_SCREWHOLE_DIAM / 2 + PICKUP_STAND_TOPHOLES_DIST;
    length_cutout_1 = pickup_screw_offsets[1] - pickup_screw_offsets[0] - PICKUP_SCREWHOLE_DIAM - 2 * PICKUP_STAND_TOPHOLES_DIST;
    offset_cutout_2 = pickup_screw_offsets[1] + PICKUP_SCREWHOLE_DIAM / 2 + PICKUP_STAND_TOPHOLES_DIST;
    length_cutout_2 = pickup_screw_offsets[2] - pickup_screw_offsets[1] - PICKUP_SCREWHOLE_DIAM - 2 * PICKUP_STAND_TOPHOLES_DIST;
    offset_cutout_3 = pickup_screw_offsets[2] + PICKUP_SCREWHOLE_DIAM / 2 + PICKUP_STAND_TOPHOLES_DIST;
    length_cutout_3 = pickup_screw_offsets[3] - pickup_screw_offsets[2] - PICKUP_SCREWHOLE_DIAM - 2 * PICKUP_STAND_TOPHOLES_DIST;
    difference() {
        translate([-PICKUP_EXTRA_LENGTH, 0, 0]) cube([PICKUP_LENGTH, PICKUP_DEPTH, PICKUP_STAND_HEIGHT]);
        translate([0, 0, PICKUP_STAND_HEIGHT - 20]) pickup_screw_cutout();
        translate([offset_cutout_1, 0, PICKUP_STAND_BOTTOM_THICKNESS]) cube([length_cutout_1, PICKUP_DEPTH, PICKUP_STAND_HEIGHT]);
        translate([offset_cutout_2, 0, PICKUP_STAND_BOTTOM_THICKNESS]) cube([length_cutout_2, PICKUP_DEPTH, PICKUP_STAND_HEIGHT]);
        translate([offset_cutout_3, 0, PICKUP_STAND_BOTTOM_THICKNESS]) cube([length_cutout_3, PICKUP_DEPTH, PICKUP_STAND_HEIGHT]);
        translate([offset_cutout_1 + length_cutout_1 / 3, PICKUP_DEPTH / 2, 0]) cylinder(h=PICKUP_STAND_BOTTOM_THICKNESS, d=PICKUP_STAND_SCREWHOLES_DIAM);
        translate([offset_cutout_2 + length_cutout_2 / 2, PICKUP_DEPTH / 2, 0]) cylinder(h=PICKUP_STAND_BOTTOM_THICKNESS, d=PICKUP_STAND_SCREWHOLES_DIAM);
        translate([offset_cutout_3 + length_cutout_3 / 2, PICKUP_DEPTH / 2, 0]) cylinder(h=PICKUP_STAND_BOTTOM_THICKNESS, d=PICKUP_STAND_SCREWHOLES_DIAM);
    }
}
