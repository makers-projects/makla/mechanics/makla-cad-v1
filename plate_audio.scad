$fa=1;
$fs=0.3;

include <BOSL2/std.scad>


PLATE_TOP_THICKNESS = 3;
PITCH_POTENTIOMETERS = 30;
PITCH_POTENTIOMETERS_JACKS = 23;
PITCH_JACKS = 20;
PITCH_CHANNELS = 24;
PITCH_MASTER = 30;
OFFSET_POTENTIOMETERS =35;

PLATE_TOP_WIDTH = PITCH_CHANNELS * 9 + PITCH_MASTER;
PLATE_TOP_HEIGHT = OFFSET_POTENTIOMETERS + PITCH_POTENTIOMETERS + PITCH_POTENTIOMETERS_JACKS + PITCH_JACKS * 2;

FONTSIZE_CHANNEL = 8;
FONTSIZE_FUNCTION = 6;
FONT_DEPTH = 1;
POTENTIOMETER_KNOB_DIAM = 18;

SCREW_DIAM = 3.4;

module cutout_potentiometer(shaft_diam, pin_distance, pin_length, pin_thickness, pin_height) {
    cylinder(30, d=shaft_diam);
    translate([pin_distance - pin_thickness / 2, -pin_length / 2, 0]) cube([pin_thickness, pin_length, pin_height]);
}

module cutout_channel() {
    translate([0, PLATE_TOP_HEIGHT, 0]) {
        translate([0, -OFFSET_POTENTIOMETERS, 0]) cutout_potentiometer(7.6, 8, 3.5, 2.2, 2);
        translate([0, -(OFFSET_POTENTIOMETERS + PITCH_POTENTIOMETERS), 0]) cutout_potentiometer(7.6, 8, 3.5, 2.2, 2);
        translate([0, -(OFFSET_POTENTIOMETERS + PITCH_POTENTIOMETERS + PITCH_POTENTIOMETERS_JACKS), 0]) cylinder(30, d=9);
        translate([0,  -(OFFSET_POTENTIOMETERS + PITCH_POTENTIOMETERS + PITCH_POTENTIOMETERS_JACKS + PITCH_JACKS), 0]) cylinder(30, d=7.2);
    }
}

module cutout_screws() {
    translate([0, PLATE_TOP_HEIGHT, 0]) {
        translate([0, -(OFFSET_POTENTIOMETERS + PITCH_POTENTIOMETERS + PITCH_POTENTIOMETERS_JACKS), 0]) cylinder(30, d=SCREW_DIAM);
        translate([0,  -(OFFSET_POTENTIOMETERS + PITCH_POTENTIOMETERS + PITCH_POTENTIOMETERS_JACKS + PITCH_JACKS), 0]) cylinder(30, d=SCREW_DIAM);
    }
}

module labels_channel(ch_name) {
    translate([0, PLATE_TOP_HEIGHT, 0]) {
        translate([0, -FONTSIZE_CHANNEL * 1.2, 0]) text3d(ch_name, size=FONTSIZE_CHANNEL, h=FONT_DEPTH + 1, font="Quicksand:style=Bold", anchor=CENTER+BOTTOM);
        translate([0, -(OFFSET_POTENTIOMETERS - POTENTIOMETER_KNOB_DIAM / 2), 0]) text3d("Vol", size=FONTSIZE_FUNCTION, h=FONT_DEPTH + 1, font="Quicksand:style=Bold", anchor=CENTER+BOTTOM);
        translate([0, -(OFFSET_POTENTIOMETERS + PITCH_POTENTIOMETERS - POTENTIOMETER_KNOB_DIAM / 2), 0]) text3d("Tone", font="Quicksand:style=Bold", size=FONTSIZE_FUNCTION, h=FONT_DEPTH + 1, anchor=CENTER+BOTTOM);
    }
}

module plate_top() {
    difference() {
        cube([PLATE_TOP_WIDTH, PLATE_TOP_HEIGHT, PLATE_TOP_THICKNESS]);
        
        for (nr_channel = [0:7]) {
            translate([PITCH_CHANNELS * (1 + nr_channel), 0, 0]) cutout_channel();
            translate([PITCH_CHANNELS * (0.5 + nr_channel), 0, 0]) cutout_screws();
        }

        translate([PITCH_CHANNELS * 8 + PITCH_MASTER, 0, 0]) cutout_channel();
        translate([PITCH_CHANNELS * 8 + PITCH_MASTER / 2, 0, 0]) cutout_screws();
        translate([PITCH_CHANNELS * 8 + PITCH_MASTER + PITCH_CHANNELS / 2, 0, 0]) cutout_screws();
    }
    
    for (nr_channel = [0:7]) {
        translate([PITCH_CHANNELS * (1 + nr_channel), 0, PLATE_TOP_THICKNESS]) labels_channel(str("Ch", nr_channel + 1));
    }
    translate([PITCH_CHANNELS * 8 + PITCH_MASTER, 0, PLATE_TOP_THICKNESS]) labels_channel("Master");
}
