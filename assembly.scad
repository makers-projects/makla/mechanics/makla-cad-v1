include <keys.scad>

module pickup() {
    pickup_stand();
    translate([0, 0, PICKUP_STAND_HEIGHT + 20 ]) pickup_cover_bottom();
    translate([0, 0, PICKUP_STAND_HEIGHT + 20 + PICKUP_COVER_HEIGHT + 2]) pickup_core();
    translate([0, 0, PICKUP_STAND_HEIGHT + 20 + PICKUP_COVER_HEIGHT + 2 + PICKUP_CORE_HEIGHT + 2]) pickup_cover_top();
}

pickup_core();
